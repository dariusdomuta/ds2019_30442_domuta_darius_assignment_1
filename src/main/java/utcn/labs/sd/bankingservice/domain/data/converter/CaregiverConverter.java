package utcn.labs.sd.bankingservice.domain.data.converter;

import utcn.labs.sd.bankingservice.domain.data.entity.Caregiver;
import utcn.labs.sd.bankingservice.domain.data.entity.Caregiver;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;

import java.util.ArrayList;
import java.util.List;

public class CaregiverConverter {

    private CaregiverConverter() {
    }

    public static CaregiverDTO toDto(Caregiver model) {
        CaregiverDTO dto = null;
        if (model != null) {
            dto = new CaregiverDTO(model.getId(), model.getName(), model.getBirthDate(), model.getGender(), model.getAddress(), model.getAccount());
        }
        return dto;
    }

    public static List<CaregiverDTO> toDto(List<Caregiver> models) {
        List<CaregiverDTO> caregiverDtos = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Caregiver model : models) {
                caregiverDtos.add(toDto(model));
            }
        }
        return caregiverDtos;
    }

    public static Caregiver fromDto(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = null;
        if (caregiverDTO != null) {
            caregiver = new Caregiver(caregiverDTO.getId(), caregiverDTO.getName(), caregiverDTO.getBirthDate(), caregiverDTO.getGender(), caregiverDTO.getAddress(),
                    caregiverDTO.getAccount());
        }
        return caregiver;
    }

    public static List<Caregiver> fromDto(List<CaregiverDTO> caregiverDTOs) {
        List<Caregiver> caregivers = new ArrayList<>();
        if (caregiverDTOs != null && !caregiverDTOs.isEmpty()) {
            for (CaregiverDTO caregiverDTO: caregiverDTOs){
                caregivers.add(fromDto(caregiverDTO));
            }
        }
        return caregivers;
    }
}
