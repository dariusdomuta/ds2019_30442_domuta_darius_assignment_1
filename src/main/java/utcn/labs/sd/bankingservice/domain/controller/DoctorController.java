package utcn.labs.sd.bankingservice.domain.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utcn.labs.sd.bankingservice.core.configuration.SwaggerTags;
import utcn.labs.sd.bankingservice.domain.dto.DoctorDTO;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
import utcn.labs.sd.bankingservice.domain.service.AccountService;
import utcn.labs.sd.bankingservice.domain.service.DoctorService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = {SwaggerTags.BANKING_SERVICE_TAG})
@RestController
@RequestMapping("/hospital/doctor")
@CrossOrigin
class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "getDoctorByAccount", tags = SwaggerTags.DOCTOR_TAG)
    @GetMapping(value = "getDoctorByAccount/{username}")
    public DoctorDTO getDoctorByAccount(@PathVariable String username) {
        try {
            return doctorService.getAllDoctors().stream().filter(doctor -> doctor.getAccount().getUsername().equals(username)).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "getAllDoctors", tags = SwaggerTags.DOCTOR_TAG)
    @RequestMapping(method = RequestMethod.GET, value = "getAllDoctors")
    public List<DoctorDTO> getAllDoctors() {
        return doctorService.getAllDoctors();
    }

    @ApiOperation(value = "findDoctorById", tags = SwaggerTags.DOCTOR_TAG)
    @GetMapping(value = "findDoctorById/{id_doctor}")
    public DoctorDTO findDoctorById(@PathVariable("id_doctor") Integer doctorId) {
        try {
            return doctorService.getDoctorById(doctorId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "insertDoctor", tags = SwaggerTags.DOCTOR_TAG)
    @PostMapping(value = "insertDoctor")
    public ResponseEntity<?> insertDoctor(@RequestBody DoctorDTO doctorDto) {
        DoctorDTO doctorDtoToBeInserted;
        try {
            doctorDtoToBeInserted = doctorService.createDoctor(doctorDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(doctorDtoToBeInserted, HttpStatus.OK);
    }

    @ApiOperation(value = "updateDoctor", tags = SwaggerTags.DOCTOR_TAG)
    @PutMapping(value = "updateDoctor/{id_doctor}")
    public ResponseEntity<?> updateDoctor(@PathVariable("id_doctor") Integer doctorId, @RequestBody DoctorDTO doctorDto) {
        try {
            doctorService.updateDoctor(doctorId, doctorDto);
            return new ResponseEntity<>(doctorDto, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "deleteDoctor", tags = SwaggerTags.DOCTOR_TAG)
    @DeleteMapping(value = "deleteDoctor/{id_doctor}")
    public ResponseEntity<?> deleteDoctor(@PathVariable("id_doctor") Integer doctorId) {
        try {
            DoctorDTO doctorDTO = doctorService.getDoctorById(doctorId);
            doctorService.deleteDoctor(doctorId);

            return new ResponseEntity<Integer>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}