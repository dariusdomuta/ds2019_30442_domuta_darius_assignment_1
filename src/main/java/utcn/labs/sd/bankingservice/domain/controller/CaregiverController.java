package utcn.labs.sd.bankingservice.domain.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utcn.labs.sd.bankingservice.core.configuration.SwaggerTags;
import utcn.labs.sd.bankingservice.domain.dto.AccountDTO;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;
import utcn.labs.sd.bankingservice.domain.dto.DoctorDTO;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
import utcn.labs.sd.bankingservice.domain.service.AccountService;
import utcn.labs.sd.bankingservice.domain.service.CaregiverService;
import utcn.labs.sd.bankingservice.domain.service.DoctorService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = {SwaggerTags.BANKING_SERVICE_TAG})
@RestController
@RequestMapping("/hospital/caregiver")
@CrossOrigin
class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "getCaregiverByAccount", tags = SwaggerTags.CAREGIVER_TAG)
    @GetMapping(value = "getCaregiverByAccount/{username}")
    public CaregiverDTO getCaregiverByAccount(@PathVariable String username) {
        try {
            return caregiverService.getAllCaregivers().stream().filter(caregiver -> caregiver.getAccount().getUsername().equals(username)).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "getAllCaregivers", tags = SwaggerTags.CAREGIVER_TAG)
    @RequestMapping(method = RequestMethod.GET, value = "getAllCaregivers")
    public List<CaregiverDTO> getAllCaregivers() {
        return caregiverService.getAllCaregivers();
    }

    @ApiOperation(value = "findCaregiverById", tags = SwaggerTags.CAREGIVER_TAG)
    @GetMapping(value = "findCaregiverById/{id_caregiver}")
    public CaregiverDTO findCaregiverById(@PathVariable("id_caregiver") Integer caregiverId) {
        try {
            return caregiverService.getCaregiverById(caregiverId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "findCaregiverPatients", tags = SwaggerTags.CAREGIVER_TAG)
    @GetMapping(value = "findCaregiverPatients/{id_caregiver}")
    public List<PatientDTO> findCaregiverPatients(@PathVariable("id_caregiver") Integer caregiverId) {
        try {
            return caregiverService.getCaregiverPatients(caregiverId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "insertCaregiver", tags = SwaggerTags.CAREGIVER_TAG)
    @PostMapping(value = "insertCaregiver")
    public ResponseEntity<?> insertCaregiver(@RequestBody CaregiverDTO caregiverDto) {
        CaregiverDTO caregiverDtoToBeInserted;
        try {
            caregiverDtoToBeInserted = caregiverService.createCaregiver(caregiverDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(caregiverDtoToBeInserted, HttpStatus.OK);
    }

    @ApiOperation(value = "updateCaregiver", tags = SwaggerTags.CAREGIVER_TAG)
    @PutMapping(value = "updateCaregiver/{id_caregiver}")
    public ResponseEntity<?> updateCaregiver(@PathVariable("id_caregiver") Integer caregiverId, @RequestBody CaregiverDTO caregiverDto) {
        try {
            caregiverService.updateCaregiver(caregiverId, caregiverDto);
            return new ResponseEntity<>(caregiverDto, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "deleteCaregiver", tags = SwaggerTags.CAREGIVER_TAG)
    @DeleteMapping(value = "deleteCaregiver/{id_caregiver}")
    public ResponseEntity<?> deleteCaregiver(@PathVariable("id_caregiver") Integer caregiverId) {
        try {
            CaregiverDTO caregiverDTO = caregiverService.getCaregiverById(caregiverId);
            caregiverService.deleteCaregiver(caregiverId);

            return new ResponseEntity<Integer>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}