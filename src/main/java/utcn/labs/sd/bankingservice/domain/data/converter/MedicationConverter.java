package utcn.labs.sd.bankingservice.domain.data.converter;

import utcn.labs.sd.bankingservice.domain.data.entity.Medication;
import utcn.labs.sd.bankingservice.domain.dto.MedicationDTO;

import java.util.ArrayList;
import java.util.List;

public class MedicationConverter {

    private MedicationConverter() {
    }

    public static MedicationDTO toDto(Medication model) {
        MedicationDTO dto = null;
        if (model != null) {
            dto = new MedicationDTO(model.getId(), model.getName(), model.getSideEffects(), model.getDosage());
        }
        return dto;
    }

    public static List<MedicationDTO> toDto(List<Medication> models) {
        List<MedicationDTO> medicationDtos = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Medication model : models) {
                medicationDtos.add(toDto(model));
            }
        }
        return medicationDtos;
    }
}
