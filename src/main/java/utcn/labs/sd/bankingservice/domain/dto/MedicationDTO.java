package utcn.labs.sd.bankingservice.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MedicationDTO {

    @JsonProperty("medication_id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("side_effects")
    private String sideEffects;
    @JsonProperty("dosage")
    private int dosage;

    @JsonCreator
    public MedicationDTO(@JsonProperty("medication_id") Integer id,
                         @JsonProperty("name") String name,
                         @JsonProperty("side_effects") String sideEffects,
                         @JsonProperty("dosage") Integer dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    @JsonProperty("medication_id")
    public int getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("side_effects")
    public String getSideEffects() {
        return sideEffects;
    }

    @JsonProperty("dosage")
    public int getDosage() {
        return dosage;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }
}


