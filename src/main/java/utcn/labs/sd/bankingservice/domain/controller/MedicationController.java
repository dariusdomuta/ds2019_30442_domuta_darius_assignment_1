package utcn.labs.sd.bankingservice.domain.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utcn.labs.sd.bankingservice.core.configuration.SwaggerTags;
import utcn.labs.sd.bankingservice.domain.dto.MedicationDTO;
import utcn.labs.sd.bankingservice.domain.service.AccountService;
import utcn.labs.sd.bankingservice.domain.service.MedicationService;
import utcn.labs.sd.bankingservice.domain.service.PatientService;

import java.util.List;


@Api(tags = {SwaggerTags.BANKING_SERVICE_TAG})
@RestController
@RequestMapping("/hospital/medication")
@CrossOrigin
public class MedicationController {

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PatientService patientService;

    @ApiOperation(value = "getAllMedications", tags = SwaggerTags.MEDICATION_TAG)
    @RequestMapping(method = RequestMethod.GET, value = "getAllMedications")
    public List<MedicationDTO> getAllMedications() {
        return medicationService.getAllMedications();
    }

    @ApiOperation(value = "findMedicationById", tags = SwaggerTags.MEDICATION_TAG)
    @GetMapping(value = "findMedicationById/{id_medication}")
    public MedicationDTO findMedicationById(@PathVariable("id_medication") Integer medicationId) {
        try {
            return medicationService.getMedicationById(medicationId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "insertMedication", tags = SwaggerTags.MEDICATION_TAG)
    @PostMapping(value = "insertMedication")
    public ResponseEntity<?> insertMedication(@RequestBody MedicationDTO medicationDto) {
        MedicationDTO medicationDtoToBeInserted;
        try {
            medicationDtoToBeInserted = medicationService.createMedication(medicationDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(medicationDtoToBeInserted, HttpStatus.CREATED);
    }

    @ApiOperation(value = "updateMedication", tags = SwaggerTags.MEDICATION_TAG)
    @PutMapping(value = "updateMedication/{id_medication}")
    public ResponseEntity<?> updateMedication(@PathVariable("id_medication") Integer medicationId, @RequestBody MedicationDTO medicationDto) {
        try {
            medicationService.updateMedication(medicationId, medicationDto);
            return new ResponseEntity<>(medicationDto, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "deleteMedication", tags = SwaggerTags.MEDICATION_TAG)
    @DeleteMapping(value = "deleteMedication/{id_medication}")
    public ResponseEntity<?> deleteMedication(@PathVariable("id_medication") Integer medicationId) {
        try {
            medicationService.deleteMedication(medicationId);
            return new ResponseEntity<Integer>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}

