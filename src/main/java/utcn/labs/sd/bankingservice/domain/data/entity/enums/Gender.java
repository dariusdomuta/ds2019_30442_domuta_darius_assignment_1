package utcn.labs.sd.bankingservice.domain.data.entity.enums;

public enum Gender {
    MALE,
    FEMALE
}
