package utcn.labs.sd.bankingservice.domain.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import utcn.labs.sd.bankingservice.domain.data.entity.Caregiver;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {
}
