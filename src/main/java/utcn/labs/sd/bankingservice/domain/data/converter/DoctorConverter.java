package utcn.labs.sd.bankingservice.domain.data.converter;

import utcn.labs.sd.bankingservice.domain.data.entity.Doctor;
import utcn.labs.sd.bankingservice.domain.data.entity.Doctor;
import utcn.labs.sd.bankingservice.domain.dto.DoctorDTO;
import utcn.labs.sd.bankingservice.domain.dto.DoctorDTO;

import java.util.ArrayList;
import java.util.List;

public class DoctorConverter {

    private DoctorConverter() {
    }

    public static DoctorDTO toDto(Doctor model) {
        DoctorDTO dto = null;
        if (model != null) {
            dto = new DoctorDTO(model.getId(), model.getName(), model.getBirthDate(), model.getGender(), model.getAddress(), model.getAccount());
        }
        return dto;
    }

    public static List<DoctorDTO> toDto(List<Doctor> models) {
        List<DoctorDTO> doctorDtos = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Doctor model : models) {
                doctorDtos.add(toDto(model));
            }
        }
        return doctorDtos;
    }

    public static Doctor fromDto(DoctorDTO doctorDTO) {
        Doctor doctor = null;
        if (doctorDTO != null) {
            doctor = new Doctor(doctorDTO.getId(), doctorDTO.getName(), doctorDTO.getBirthDate(), doctorDTO.getGender(), doctorDTO.getAddress(),
                    doctorDTO.getAccount());
        }
        return doctor;
    }

    public static List<Doctor> fromDto(List<DoctorDTO> doctorDTOs) {
        List<Doctor> doctors = new ArrayList<>();
        if (doctorDTOs != null && !doctorDTOs.isEmpty()) {
            for (DoctorDTO doctorDTO: doctorDTOs){
                doctors.add(fromDto(doctorDTO));
            }
        }
        return doctors;
    }
}
