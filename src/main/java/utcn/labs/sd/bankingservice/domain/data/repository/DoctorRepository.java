package utcn.labs.sd.bankingservice.domain.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import utcn.labs.sd.bankingservice.domain.data.entity.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
}
