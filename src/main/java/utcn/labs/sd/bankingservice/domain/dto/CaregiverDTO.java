package utcn.labs.sd.bankingservice.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import utcn.labs.sd.bankingservice.domain.data.entity.Account;
import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
import utcn.labs.sd.bankingservice.domain.data.entity.enums.Gender;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaregiverDTO {

    @JsonProperty("caregiver_id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("birth_date")
    private String birthDate;

    @JsonProperty("gender")
    private Gender gender;

    @JsonProperty("address")
    private String address;

    @JsonProperty("account")
    private Account account;

    @JsonProperty("patients")
    private List<PatientDTO> patients;

    @JsonCreator
    public CaregiverDTO(@JsonProperty("caregiver_id") int id,
                        @JsonProperty("name") String name,
                        @JsonProperty("birth_date") String birthDate,
                        @JsonProperty("gender") Gender gender,
                        @JsonProperty("address") String address,
                        @JsonProperty("account") Account account) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.account = account;
    }

    @JsonProperty("caregiver_id")
    public int getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("birth_date")
    public String getBirthDate() {
        return birthDate;
    }

    @JsonProperty("gender")
    public Gender getGender() {
        return gender;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("account")
    public Account getAccount() {
        return account;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "CaregiverDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender=" + gender +
                ", address='" + address + '\'' +
                '}';
    }
}

