package utcn.labs.sd.bankingservice.domain.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import utcn.labs.sd.bankingservice.core.configuration.SwaggerTags;
import utcn.labs.sd.bankingservice.domain.dto.AccountDTO;
import utcn.labs.sd.bankingservice.domain.service.AccountService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = {SwaggerTags.BANKING_SERVICE_TAG})
@RestController
@RequestMapping("/hospital/account")
@CrossOrigin
public class AccountController {

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "login", tags = SwaggerTags.ACCOUNT_TAG)
    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody AccountDTO accountDto) {
        try {
            AccountDTO accountDTOtoSend = accountService.getAllAccounts().stream().filter(account -> account.getUsername().equals(accountDto.getUsername())
                    && account.getPassword().equals(accountDto.getPassword())).collect(Collectors.toList()).get(0);
            return new ResponseEntity<>(accountDTOtoSend, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "getAllAccounts", tags = SwaggerTags.ACCOUNT_TAG)
    @RequestMapping(method = RequestMethod.GET, value = "getAllAccounts")
    public List<AccountDTO> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @ApiOperation(value = "findAccountById", tags = SwaggerTags.ACCOUNT_TAG)
    @GetMapping(value = "findAccountById/{accountId}")
    public AccountDTO findAccountById(@PathVariable("accountId") Integer accountId) {
        try {
            return accountService.getAccountById(accountId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "insertAccount", tags = SwaggerTags.ACCOUNT_TAG)
    @PostMapping(value = "insertAccount")
    public ResponseEntity<?> insertAccount(@RequestBody AccountDTO accountDto) {
        AccountDTO accountDtoToBeInserted;
        try {
            accountDtoToBeInserted = accountService.createAccount(accountDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(accountDtoToBeInserted, HttpStatus.CREATED);
    }

    @ApiOperation(value = "updateAccount", tags = SwaggerTags.ACCOUNT_TAG)
    @PutMapping(value = "updateAccount/{accountId}")
    public ResponseEntity<?> updateAccount(@PathVariable("accountId") Integer accountId, @RequestBody AccountDTO accountDto) {
        try {
            accountService.updateAccount(accountId, accountDto);
            return new ResponseEntity<>(accountDto, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "deleteAccount", tags = SwaggerTags.ACCOUNT_TAG)
    @DeleteMapping(value = "deleteAccount/{accountId}")
    public ResponseEntity<?> deleteAccount(@PathVariable("accountId") Integer accountId) {
        try {
            accountService.deleteAccount(accountId);
            return new ResponseEntity<Integer>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
