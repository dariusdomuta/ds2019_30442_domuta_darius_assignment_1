package utcn.labs.sd.bankingservice.domain.data.converter;

import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;

import java.util.ArrayList;
import java.util.List;

public class PatientConverter {

    private PatientConverter() {
    }

    public static PatientDTO toDto(Patient model) {
        PatientDTO dto = null;
        if (model != null) {
            dto = new PatientDTO(model.getId(), model.getName(), model.getBirthDate(), model.getGender(), model.getAddress(), model.getAccount(),
                    CaregiverConverter.toDto(model.getCaregiver()), model.getMedicalRecord());
        }
        return dto;
    }

    public static List<PatientDTO> toDto(List<Patient> models) {
        List<PatientDTO> patientDtos = new ArrayList<>();
        if ((models != null) && (!models.isEmpty())) {
            for (Patient model : models) {
                patientDtos.add(toDto(model));
            }
        }
        return patientDtos;
    }

    public static Patient fromDto(PatientDTO patientDTO) {
        Patient patient = null;
        if (patientDTO != null) {
            patient = new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getBirthDate(), patientDTO.getGender(), patientDTO.getAddress(),
                    patientDTO.getAccount(), patientDTO.getMedicalRecord());
        }
        return patient;
    }

    public static List<Patient> fromDto(List<PatientDTO> patientDTOs) {
        List<Patient> patients = new ArrayList<>();
        if (patientDTOs != null && !patientDTOs.isEmpty()) {
            for (PatientDTO patientDTO: patientDTOs){
                patients.add(fromDto(patientDTO));
            }
        }
        return patients;
    }


}
