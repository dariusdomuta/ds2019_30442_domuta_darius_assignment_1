package utcn.labs.sd.bankingservice.domain.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utcn.labs.sd.bankingservice.domain.data.converter.CaregiverConverter;
import utcn.labs.sd.bankingservice.domain.data.converter.PatientConverter;
import utcn.labs.sd.bankingservice.domain.data.entity.Account;
import utcn.labs.sd.bankingservice.domain.data.entity.Caregiver;
import utcn.labs.sd.bankingservice.domain.data.repository.AccountRepository;
import utcn.labs.sd.bankingservice.domain.data.repository.CaregiverRepository;
import utcn.labs.sd.bankingservice.domain.data.repository.PatientRepository;
import utcn.labs.sd.bankingservice.domain.dto.AccountDTO;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
import utcn.labs.sd.bankingservice.domain.exception.CreateClientException;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PatientService patientService;

    public List<CaregiverDTO> getAllCaregivers() {
        return CaregiverConverter.toDto(caregiverRepository.findAll());
    }

    public List<PatientDTO> getCaregiverPatients(Integer caregiverId) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverId).orElse(null);
        if (caregiver == null) throw new NotFoundException("No caregiver found with that caregiverId");
        return PatientConverter.toDto(caregiver.getPatients());
    }

    public CaregiverDTO getCaregiverById(Integer caregiverId) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverId).orElse(null);
        if (caregiver == null) throw new NotFoundException("No caregiver found with that caregiverId");
        return CaregiverConverter.toDto(caregiver);
    }

    public CaregiverDTO createCaregiver(CaregiverDTO caregiverDto) throws Exception {
        Caregiver caregiver = new Caregiver(caregiverDto.getId(), caregiverDto.getName(), caregiverDto.getBirthDate(), caregiverDto.getGender(), caregiverDto.getAddress(),
                caregiverDto.getAccount());
        Caregiver possibleAlreadyExistingCaregiver = caregiverRepository.findById(caregiverDto.getId()).orElse(null);
        if (possibleAlreadyExistingCaregiver == null) {
            Caregiver newCaregiver = caregiverRepository.save(caregiver);
            return CaregiverConverter.toDto(newCaregiver);
        } else {
            throw new CreateClientException("Caregiver already exists!");
        }

    }

    public CaregiverDTO updateCaregiver(Integer caregiverId, CaregiverDTO caregiverDto) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverId).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver found with that id");
        }
        caregiver.setId(caregiverId);
        caregiver.setAddress(caregiverDto.getAddress());
        caregiver.setBirthDate(caregiverDto.getBirthDate());
        caregiver.setGender(caregiverDto.getGender());
        caregiver.setName(caregiverDto.getName());
        return CaregiverConverter.toDto(caregiverRepository.save(caregiver));
    }

    public void deleteCaregiver(Integer caregiverId) throws Exception {
        Caregiver caregiver = caregiverRepository.findById(caregiverId).orElse(null);
        if (caregiver == null) {
            throw new NotFoundException("No caregiver with that caregiverId");
        }
        caregiverRepository.delete(caregiver);
    }

    public CaregiverDTO findCaregiverByName(String caregiverName) {
        List<Caregiver> caregivers = caregiverRepository.findAll().stream().filter(caregiver ->
                caregiver.getName().equals(caregiverName)).collect(Collectors.toList());
        if (!caregivers.isEmpty()) {
            return CaregiverConverter.toDto(caregivers.get(0));
        } else {
            return null;
        }
    }
}

