package utcn.labs.sd.bankingservice.domain.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utcn.labs.sd.bankingservice.domain.data.converter.CaregiverConverter;
import utcn.labs.sd.bankingservice.domain.data.converter.PatientConverter;
import utcn.labs.sd.bankingservice.domain.data.entity.Account;
import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
import utcn.labs.sd.bankingservice.domain.data.repository.AccountRepository;
import utcn.labs.sd.bankingservice.domain.data.repository.PatientRepository;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
import utcn.labs.sd.bankingservice.domain.exception.CreateClientException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CaregiverService caregiverService;

    public List<PatientDTO> getAllPatients() {
        return PatientConverter.toDto(patientRepository.findAll());
    }

    public PatientDTO getPatientById(int patientId) throws Exception {
        Patient patient = patientRepository.findById(patientId).orElse(null);
        if (patient == null) throw new NotFoundException("No patient found with that patientId");
        return PatientConverter.toDto(patient);
    }

    public PatientDTO createPatient(PatientDTO patientDto) throws Exception {
        Patient patient = new Patient(patientDto.getId(), patientDto.getName(), patientDto.getBirthDate(), patientDto.getGender(), patientDto.getAddress(),
                patientDto.getAccount(), patientDto.getMedicalRecord());
        Patient possibleAlreadyExistingPatient = patientRepository.findById(patientDto.getId()).orElse(null);
        if (possibleAlreadyExistingPatient == null) {
            Patient newPatient = patientRepository.save(patient);
            return PatientConverter.toDto(newPatient);
        } else {
            throw new CreateClientException("Patient already exists!");
        }
    }

    public PatientDTO changePatient(int patientId, PatientDTO patientDto) throws Exception {
        Patient patient = patientRepository.findById(patientId).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient found with that patientId");
        }
        patient.setId(patientDto.getId());
        patient.setAddress(patientDto.getAddress());
        patient.setBirthDate(patientDto.getBirthDate());
        patient.setGender(patientDto.getGender());
        patient.setName(patientDto.getName());
        patient.setCaregiver(CaregiverConverter.fromDto(patientDto.getCaregiver()));
        patient.setMedicalRecord(patientDto.getMedicalRecord());
        return PatientConverter.toDto(patientRepository.save(patient));
    }

    public void deletePatient(int patientId) throws Exception {
        Patient patient = patientRepository.findById(patientId).orElse(null);
        if (patient == null) {
            throw new NotFoundException("No patient with that patientId");
        }
        Account account = patient.getAccount();

        patientRepository.delete(patient);
    }

    public PatientDTO assignCaregiverToPatient(String caregiverName, String patientName) {
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverByName(caregiverName);
        PatientDTO patientDTO = findPatientByName(patientName);

        if (caregiverDTO != null && patientDTO != null) {
            patientDTO.setCaregiver(caregiverDTO);
            try {
                changePatient(patientDTO.getId(), patientDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return patientDTO;
        } else {
            return null;
        }
    }

    public PatientDTO findPatientByName(String patientName) {
        List<Patient> patients = patientRepository.findAll().stream().filter(patient ->
                patient.getName().equals(patientName)).collect(Collectors.toList());
        if (!patients.isEmpty()) {
            return PatientConverter.toDto(patients.get(0));
        } else {
            return null;
        }
    }

}
