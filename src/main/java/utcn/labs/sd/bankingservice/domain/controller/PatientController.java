package utcn.labs.sd.bankingservice.domain.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utcn.labs.sd.bankingservice.core.configuration.SwaggerTags;
import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
import utcn.labs.sd.bankingservice.domain.dto.AccountDTO;
import utcn.labs.sd.bankingservice.domain.dto.CaregiverDTO;
import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
import utcn.labs.sd.bankingservice.domain.service.AccountService;
import utcn.labs.sd.bankingservice.domain.service.PatientService;

import java.util.List;
import java.util.stream.Collectors;

@Api(tags = {SwaggerTags.BANKING_SERVICE_TAG})
@RestController
@RequestMapping("/hospital/patient")
@CrossOrigin
class PatientController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "getPatientByAccount", tags = SwaggerTags.PATIENT_TAG)
    @GetMapping(value = "getPatientByAccount/{username}")
    public PatientDTO getPatientByAccount(@PathVariable String username) {
        try {
            return patientService.getAllPatients().stream().filter(patient -> patient.getAccount().getUsername().equals(username)).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "getAllPatients", tags = SwaggerTags.PATIENT_TAG)
    @GetMapping(value = "getAllPatients")
    public List<PatientDTO> getAllPatients() {
        return patientService.getAllPatients();
    }


    @ApiOperation(value = "findPatientById", tags = SwaggerTags.PATIENT_TAG)
    @GetMapping(value = "findPatientById/{patientId}")
    public PatientDTO findPatientById(@PathVariable("patientId") int patientId) {
        try {
            return patientService.getPatientById(patientId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value = "insertPatient", tags = SwaggerTags.PATIENT_TAG)
    @PostMapping(value = "insertPatient")
    public ResponseEntity<?> insertPatient(@RequestBody PatientDTO patientDto) {

        PatientDTO patientDtoToBeInserted;
        try {
            patientDtoToBeInserted = patientService.createPatient(patientDto);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(patientDtoToBeInserted, HttpStatus.CREATED);
    }

    @ApiOperation(value = "updatePatient", tags = SwaggerTags.PATIENT_TAG)
    @PutMapping(value = "updatePatient/{patientId}")
    public ResponseEntity<?> updatePatient(@PathVariable("patientId") int patientId, @RequestBody PatientDTO patientDto) {

        try {
            PatientDTO changedPatient = patientService.changePatient(patientId, patientDto);
            return new ResponseEntity<>(changedPatient, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "deletePatient", tags = SwaggerTags.PATIENT_TAG)
    @DeleteMapping(value = "deletePatient/{patientId}")
    public ResponseEntity<?> deletePatient(@PathVariable("patientId") int patientId) {

        try {
            PatientDTO patientDto = patientService.getPatientById(patientId);

            patientService.deletePatient(patientId);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (NotFoundException ne) {
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "assignCaregiverToPatient", tags = SwaggerTags.PATIENT_TAG)
    @PostMapping(value = "/assignCaregiverToPatient/{caregiverName}/{patientName}")
    public ResponseEntity<?> assignCaregiverToPatient(@PathVariable("caregiverName") String caregiverName,
                                                   @PathVariable("patientName") String patientName) {
        PatientDTO patientDTO = patientService.assignCaregiverToPatient(caregiverName, patientName);
        if (patientDTO != null) {

            return new ResponseEntity<>(patientDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Caregiver or patient not found!", HttpStatus.BAD_REQUEST);
        }
    }
}
