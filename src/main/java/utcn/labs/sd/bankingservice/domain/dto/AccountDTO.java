package utcn.labs.sd.bankingservice.domain.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import utcn.labs.sd.bankingservice.domain.data.entity.Caregiver;
import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
import utcn.labs.sd.bankingservice.domain.data.entity.enums.AccountType;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDTO {
    private int id;
    private AccountType accountType;
    private String username;
    private String password;

    @JsonCreator
    public AccountDTO(@JsonProperty("account_id") int id,
                      @JsonProperty("account_type") AccountType accountType,
                      @JsonProperty("username") String username,
                      @JsonProperty("password") String password) {
        this.id = id;
        this.accountType = accountType;
        this.username = username;
        this.password = password;
    }

    @JsonProperty("account_id")
    public int getAccountId() {return id;}

    @JsonProperty("account_type")
    public AccountType getAccountType() {
        return accountType;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                ", accountType=" + accountType +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
