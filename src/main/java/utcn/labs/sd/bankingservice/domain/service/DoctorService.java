package utcn.labs.sd.bankingservice.domain.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utcn.labs.sd.bankingservice.domain.data.converter.DoctorConverter;
import utcn.labs.sd.bankingservice.domain.data.entity.Account;
import utcn.labs.sd.bankingservice.domain.data.entity.Doctor;
import utcn.labs.sd.bankingservice.domain.data.repository.AccountRepository;
import utcn.labs.sd.bankingservice.domain.data.repository.DoctorRepository;
import utcn.labs.sd.bankingservice.domain.dto.DoctorDTO;
import utcn.labs.sd.bankingservice.domain.exception.CreateClientException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PatientService patientService;

    public List<DoctorDTO> getAllDoctors() {
        return DoctorConverter.toDto(doctorRepository.findAll());
    }

    public DoctorDTO getDoctorById(Integer doctorId) throws Exception {
        Doctor doctor = doctorRepository.findById(doctorId).orElse(null);
        if (doctor == null) throw new NotFoundException("No doctor found with that doctorId");
        return DoctorConverter.toDto(doctor);
    }

    public DoctorDTO createDoctor(DoctorDTO doctorDto) throws Exception {
        Doctor doctor = new Doctor(doctorDto.getId(), doctorDto.getName(), doctorDto.getBirthDate(), doctorDto.getGender(), doctorDto.getAddress(),
                doctorDto.getAccount());
        Doctor possibleAlreadyExistingDoctor = doctorRepository.findById(doctorDto.getId()).orElse(null);
        if (possibleAlreadyExistingDoctor == null) {
            Doctor newDoctor = doctorRepository.save(doctor);
            return DoctorConverter.toDto(newDoctor);
        } else {
            throw new CreateClientException("Doctor already exists!");
        }

    }

    public DoctorDTO updateDoctor(Integer doctorId, DoctorDTO doctorDto) throws Exception {
        Doctor doctor = doctorRepository.findById(doctorId).orElse(null);
        if (doctor == null) {
            throw new NotFoundException("No doctor found with that id");
        }
        doctor.setId(doctorId);
        doctor.setAddress(doctorDto.getAddress());
        doctor.setBirthDate(doctorDto.getBirthDate());
        doctor.setGender(doctorDto.getGender());
        doctor.setName(doctorDto.getName());
        return DoctorConverter.toDto(doctorRepository.save(doctor));
    }

    public void deleteDoctor(Integer doctorId) throws Exception {
        Doctor doctor = doctorRepository.findById(doctorId).orElse(null);
        if (doctor == null) {
            throw new NotFoundException("No doctor with that doctorId");
        }

        doctorRepository.delete(doctor);
    }

    public DoctorDTO findDoctorByName(String doctorName) {
        List<Doctor> doctors = doctorRepository.findAll().stream().filter(doctor ->
                doctor.getName().equals(doctorName)).collect(Collectors.toList());
        if (!doctors.isEmpty()) {
            return DoctorConverter.toDto(doctors.get(0));
        } else {
            return null;
        }
    }
}

