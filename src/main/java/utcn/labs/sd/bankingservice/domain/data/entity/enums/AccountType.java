package utcn.labs.sd.bankingservice.domain.data.entity.enums;

public enum AccountType {
    DOCTOR,
    PATIENT,
    CAREGIVER,
}
