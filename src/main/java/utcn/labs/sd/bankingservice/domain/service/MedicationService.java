package utcn.labs.sd.bankingservice.domain.service;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utcn.labs.sd.bankingservice.domain.data.converter.MedicationConverter;
import utcn.labs.sd.bankingservice.domain.data.entity.Medication;
import utcn.labs.sd.bankingservice.domain.data.repository.MedicationRepository;
import utcn.labs.sd.bankingservice.domain.dto.MedicationDTO;

import java.util.List;

@Service
public class MedicationService {

    @Autowired
    private MedicationRepository medicationRepository;

    public List<MedicationDTO> getAllMedications() {
        return MedicationConverter.toDto(medicationRepository.findAll());
    }

    public MedicationDTO getMedicationById(Integer medicationId) throws Exception {
        Medication medication = medicationRepository.findById(medicationId).orElse(null);
        if (medication == null) throw new NotFoundException("No medication found with that medicationId");
        return MedicationConverter.toDto(medication);
    }

    public MedicationDTO createMedication(MedicationDTO medicationDto) throws Exception {
        Medication medication = new Medication(medicationDto.getId(), medicationDto.getName(), medicationDto.getSideEffects(), medicationDto.getDosage());
        Medication newMedication = medicationRepository.save(medication);
        return MedicationConverter.toDto(newMedication);
    }


    public MedicationDTO updateMedication(Integer medicationId, MedicationDTO medicationDto) throws Exception {
        Medication medication = medicationRepository.findById(medicationId).orElse(null);
        if (medication == null) {
            throw new NotFoundException("No medication found with that id");
        }
        medication.setDosage(medicationDto.getDosage());
        medication.setName(medicationDto.getName());
        medication.setSideEffects(medicationDto.getSideEffects());
        return MedicationConverter.toDto(medicationRepository.save(medication));
    }


    public void deleteMedication(Integer medicationId) throws Exception {
        Medication medication = medicationRepository.findById(medicationId).orElse(null);
        if (medication == null) {
            throw new NotFoundException("No medication with that medicationId");
        }
        medicationRepository.delete(medication);
    }
}
