package utcn.labs.sd.bankingservice.core.configuration;

import springfox.documentation.service.Tag;

public final class SwaggerTags {

    public static final String BANKING_SERVICE_TAG = "Hospital Service Overview";
    public static final Tag BANKING_SERVICE = new Tag(BANKING_SERVICE_TAG,
            "All hospotal related resources", 0);


    public static final String ACCOUNT_TAG = "Account";
    public static final Tag ACCOUNT = new Tag(ACCOUNT_TAG, "Resource: Account");

    public static final String PATIENT_TAG = "Patient";
    public static final Tag PATIENT = new Tag(PATIENT_TAG, "Resource: Patient");

    public static final String CAREGIVER_TAG = "Caregiver";
    public static final Tag CAREGIVER = new Tag(CAREGIVER_TAG, "Resource: Caregiver");

    public static final String DOCTOR_TAG = "Doctor";
    public static final Tag DOCTOR = new Tag(DOCTOR_TAG, "Resource: Doctor");

    public static final String MEDICATION_TAG = "Medication";
    public static final Tag MEDICATION = new Tag(MEDICATION_TAG, "Resource: Medication");


    /**
     * Don't allow class instantiation
     */
    private SwaggerTags() {
        // hide from instantiation
    }
}
