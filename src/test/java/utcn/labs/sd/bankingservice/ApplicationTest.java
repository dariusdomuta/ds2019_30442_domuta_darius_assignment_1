//package utcn.labs.sd.bankingservice;
//
//import com.fasterxml.jackson.databind.ser.std.AsArraySerializerBase;
//import junit.framework.Assert;
//import junit.framework.TestCase;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.Assert;
//import utcn.labs.sd.bankingservice.domain.controller.MedicationController;
//import utcn.labs.sd.bankingservice.domain.data.entity.Account;
//import utcn.labs.sd.bankingservice.domain.data.entity.Bill;
//import utcn.labs.sd.bankingservice.domain.data.entity.Patient;
//import utcn.labs.sd.bankingservice.domain.data.entity.enums.AccountType;
//import utcn.labs.sd.bankingservice.domain.dto.AccountDTO;
//import utcn.labs.sd.bankingservice.domain.dto.MedicationDTO;
//import utcn.labs.sd.bankingservice.domain.dto.PatientDTO;
//import utcn.labs.sd.bankingservice.domain.service.AccountService;
//import utcn.labs.sd.bankingservice.domain.service.MedicationService;
//import utcn.labs.sd.bankingservice.domain.service.PatientService;
//import utcn.labs.sd.bankingservice.domain.service.CaregiverService;
//
//import java.util.Collections;
//import java.util.List;
//
//public class ApplicationTest extends TestCase {
//
//    @Autowired
//    AccountService accountService = new AccountService();
//
//    @Autowired
//    MedicationService billService = new MedicationService();
//
//    @Autowired
//    PatientService clientService = new PatientService();
//
//    @Autowired
//    CaregiverService employeeService = new CaregiverService();
//
//    @Test
//    void testAccountService() throws Exception {
//        AccountDTO accountDTO = new AccountDTO(5, AccountType.DOCTOR, "willBeRepalced", 400);
//
//        AccountDTO createdAccountDto = accountService.createAccount(accountDTO);
//
//        Assert.assertEquals(accountService.getAccountById(createdAccountDto.getId()).getId(), createdAccountDto.getId());
//
//        AccountDTO updatedAccount = accountService.updateAccount(createdAccountDto.getId(), new AccountDTO(createdAccountDto.getId(), createdAccountDto.getAccountType(), createdAccountDto.getUsername(), createdAccountDto.getPassword() + 100));
//
//        Assert.assertEquals(updatedAccount.getPassword(), accountService.getAccountById(createdAccountDto.getId()).getPassword());
//
//        accountService.deleteAccount(createdAccountDto.getId());
//
//        Assert.assertEquals(accountService.getAccountById(createdAccountDto.getId()), null);
//
//    }
//
//    @Test
//    void testBillService() throws Exception {
//
//        MedicationDTO billDTO = new MedicationDTO(0, "Kolozsvar", 200, false);
//
//        MedicationDTO createdBillDTO = billService.createBill(billDTO);
//
//        MedicationDTO foundBill = billService.getBillById(createdBillDTO.getIdBill());
//
//        MedicationDTO updatedBill = billService.updateBill(createdBillDTO.getIdBill(), new MedicationDTO(createdBillDTO.getIdBill(), createdBillDTO.getBillAddress(), createdBillDTO.getValue() + 100, createdBillDTO.getIsPaid()));
//
//        Assert.assertEquals(createdBillDTO.getIdBill(), foundBill.getIdBill());
//        Assert.assertEquals((Integer) (createdBillDTO.getValue() + 100), updatedBill.getValue());
//
//        billService.deleteBill(createdBillDTO.getIdBill());
//
//        Assert.assertEquals(billService.getBillById(createdBillDTO.getIdBill()), null);
//    }
//
//    void testClientService() throws Exception {
//
//        AccountDTO accountDTO = new AccountDTO(5, AccountType.DOCTOR, "willBeRepalced", 400);
//
//        AccountDTO createdAccountDto = accountService.createAccount(accountDTO);
//
//        Account account = new Account(createdAccountDto.getId(),createdAccountDto.getAccountType(), createdAccountDto.getUsername(), createdAccountDto.getPassword());
//
//        List<Account> accountList = Collections.emptyList();
//        accountList.add(account);
//
//        PatientDTO clientDTO = new PatientDTO("123abcabc", "Zeul", "Suprem", "SX112112", "Cluj-Nampoca", "eror404@yahoo.idk",accountList);
//
//        PatientDTO createClientDto = clientService.createClient(clientDTO);
//
//        Assert.assertEquals(clientService.getClientById(createClientDto.getSsn()).getSsn(), createClientDto.getSsn());
//    }
//}