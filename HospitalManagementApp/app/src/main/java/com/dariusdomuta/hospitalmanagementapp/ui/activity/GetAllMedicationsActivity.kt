package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Medication
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.get_all_medications_activity.*
import kotlinx.android.synthetic.main.medication_item_layout.view.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class GetAllMedicationsActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, GetAllMedicationsActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.get_all_medications_activity)
        HospitalManagementApp.GRAPH.inject(this)
    }

    override fun onStart() {
        super.onStart()

        getAllMedications()
    }

    fun getAllMedications() {
        rx(
            hospitalManagementApi.getAllMedications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ medications ->
                    medications.forEach {
                        displayMedication(it)
                    }
                }, {
                    showToast("Error getting medications!")
                })
        )
    }

    private fun displayMedication(medication: Medication) {
        val newMedicationLayout = layoutInflater.inflate(R.layout.medication_item_layout, null)
        newMedicationLayout.name.text = medication.name
        newMedicationLayout.sideEffects.text = medication.sideEffects
        newMedicationLayout.dosage.text = medication.dosage.toString()

        medicationsLayout.addView(newMedicationLayout)
    }
}