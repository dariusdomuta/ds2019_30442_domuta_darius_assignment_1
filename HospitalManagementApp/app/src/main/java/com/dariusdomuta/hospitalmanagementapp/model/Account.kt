package com.dariusdomuta.hospitalmanagementapp.model

import com.google.gson.annotations.SerializedName

class Account(
    @SerializedName("account_id")
    val accountId: Int,
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("account_type")
    val account_type: AccountType
)