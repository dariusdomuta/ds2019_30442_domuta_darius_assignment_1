package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.assign_caregiver_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class AssignCaregiverActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, AssignCaregiverActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.assign_caregiver_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            assignCaregiver()
        }
    }

    fun assignCaregiver() {
        rx(
            hospitalManagementApi.assignCaregiverToPatient(
                caregiverName.getText() ?: "",
                patientName.getText() ?: ""
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showToast("Caregiver assigned to patient!")
                }, {
                    showToast("Error assignment not successful!")
                })
        )
    }
}