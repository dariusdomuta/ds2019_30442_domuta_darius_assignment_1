package com.dariusdomuta.hospitalmanagementapp.network

import com.dariusdomuta.hospitalmanagementapp.model.*
import okhttp3.ResponseBody
import retrofit2.http.*
import rx.Observable

interface HospitalManagementApi {

    @POST("/hospital/account/login")
    fun login(@Body account: Account): Observable<Account>

    @GET("/hospital/account/getAllAccounts")
    fun getAllAccounts(): Observable<List<Account>>

    @GET("/hospital/account/findAccountById/{accountId}")
    fun findAccountById(@Path("accountId") accountId: Int): Observable<Account>

    @POST("/hospital/account/insertAccount")
    fun insertAccount(@Body account: Account): Observable<ResponseBody>

    @PUT("/hospital/account/updateAccount/{accountId}")
    fun updateAccount(@Path("accountId") accountId: Int, @Body account: Account) : Observable<ResponseBody>

    @DELETE("/hospital/account/deleteAccount/{accountId}")
    fun deleteAccount(@Path("accountId") accountId: Int): Observable<ResponseBody>

    @GET("/hospital/caregiver/getCaregiverByAccount/{username}")
    fun getCaregiverByAccount(@Path("username") username: String) : Observable<Caregiver>

    @GET("/hospital/caregiver/getAllCaregivers")
    fun getAllCaregivers(): Observable<List<Caregiver>>

    @GET("/hospital/caregiver/findCaregiverPatients/{id_caregiver}")
    fun findCaregiverPatients(@Path("id_caregiver") caregiverId: Int) : Observable<List<Patient>>

    @GET("/hospital/caregiver/findCaregiverById/{id_caregiver}")
    fun findCaregiverById(@Path("id_caregiver") CaregiverId: Int): Observable<Caregiver>

    @POST("/hospital/caregiver/insertCaregiver")
    fun insertCaregiver(@Body Caregiver: Caregiver): Observable<ResponseBody>

    @PUT("/hospital/caregiver/updateCaregiver/{id_caregiver}")
    fun updateCaregiver(@Path("id_caregiver") CaregiverId: Int, @Body Caregiver: Caregiver) : Observable<ResponseBody>

    @DELETE("/hospital/caregiver/deleteCaregiver/{id_caregiver}")
    fun deleteCaregiver(@Path("id_caregiver") CaregiverId: Int): Observable<ResponseBody>

    @GET("/hospital/doctor/getCaregiverByAccount/{username}")
    fun getDoctorByAccount(@Path("username") username: String) : Observable<Doctor>

    @GET("/hospital/doctor/getAllDoctors")
    fun getAllDoctors(): Observable<List<Doctor>>

    @GET("/hospital/doctor/findDoctorById/{id_doctor}")
    fun findDoctorById(@Path("id_doctor") DoctorId: Int): Observable<Doctor>

    @POST("/hospital/doctor/insertDoctor")
    fun insertDoctor(@Body Doctor: Doctor): Observable<ResponseBody>

    @PUT("/hospital/doctor/updateDoctor/{id_doctor}")
    fun updateDoctor(@Path("id_doctor") DoctorId: Int, @Body Doctor: Doctor) : Observable<ResponseBody>

    @DELETE("/hospital/doctor/deleteDoctor/{id_doctor}")
    fun deleteDoctor(@Path("id_doctor") DoctorId: Int): Observable<ResponseBody>

    @GET("/hospital/medication/getAllMedications")
    fun getAllMedications(): Observable<List<Medication>>

    @GET("/hospital/medication/findMedicationById/{id_medication}")
    fun findMedicationById(@Path("id_medication") MedicationId: Int): Observable<Medication>

    @POST("/hospital/medication/insertMedication")
    fun insertMedication(@Body Medication: Medication): Observable<ResponseBody>

    @PUT("/hospital/medication/updateMedication/{id_medication}")
    fun updateMedication(@Path("id_medication") MedicationId: Int, @Body Medication: Medication) : Observable<ResponseBody>

    @DELETE("/hospital/medication/deleteMedication/{id_medication}")
    fun deleteMedication(@Path("id_medication") MedicationId: Int): Observable<ResponseBody>

    @GET("/hospital/patient/getPatientByAccount/{username}")
    fun getPatientByAccount(@Path("username") username: String) : Observable<Patient>

    @GET("/hospital/patient/getAllPatients")
    fun getAllPatients(): Observable<List<Patient>>

    @GET("/hospital/patient/findPatientById/{patientId}")
    fun findPatientById(@Path("patientId") PatientId: Int): Observable<Patient>

    @POST("/hospital/patient/insertPatient")
    fun insertPatient(@Body Patient: Patient): Observable<ResponseBody>

    @PUT("/hospital/patient/updatePatient/{patientId}")
    fun updatePatient(@Path("patientId") PatientId: Int, @Body Patient: Patient): Observable<ResponseBody>

    @DELETE("/hospital/patient/deletePatient/{patientId}")
    fun deletePatient(@Path("patientId") PatientId: Int): Observable<ResponseBody>

    @POST("/hospital/patient/assignCaregiverToPatient/{caregiverName}/{patientName}")
    fun assignCaregiverToPatient(@Path("caregiverName") caregiverName : String,
                                 @Path ("patientName") patientName: String): Observable<ResponseBody>
}