package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.show
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.patient_activity.*
import kotlinx.android.synthetic.main.person_item_layout.view.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class PatientActivity : BaseActivity() {

    companion object {
        val USERNAME_KEY = "username_key"

        fun newInstance(context: Context, username: String): Intent {
            val intent = Intent(context, PatientActivity::class.java)
            intent.putExtra(USERNAME_KEY, username)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.patient_activity)
        HospitalManagementApp.GRAPH.inject(this)

        setupUI()

    }

    override fun onStart() {
        super.onStart()
        findPatientByAccount(userStorage.getUser()?.username ?: "")
    }

    override fun onResume() {
        super.onResume()
        unlockUI()
    }


    private fun setupUI() {
        logoutButton.show()
        loggedUserName.text = userStorage.getUser()?.username

        logoutButton.setOnClickListener {
            logoutUser()
        }
    }

    private fun logoutUser() {
        userStorage.logout()
        startActivity(LoginActivity.newInstance(this))
        finish()
    }

    fun findPatientByAccount(username: String) {
        rx(
            hospitalManagementApi.getPatientByAccount(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ patient ->
                    displayPatient(patient)
                }, {
                    showToast("Error patient not found!")
                })
        )
    }

    private fun displayPatient(patient: Patient) {
        val newPersonLayout = layoutInflater.inflate(R.layout.person_item_layout, null)
        newPersonLayout.username.text = patient.account.username
        newPersonLayout.password.text = patient.account.password
        newPersonLayout.accountType.text = "PATIENT"
        newPersonLayout.name.text = patient.name
        newPersonLayout.birthDate.text = patient.birthDate
        newPersonLayout.address.text = patient.address
        newPersonLayout.gender.text = patient.gender.toString()
        newPersonLayout.medicalRecord.text = patient.medicalRecord
        newPersonLayout.caregiverName.text = patient.caregiver?.name ?: "No Caregiver Assigned"

        patientsLayout.addView(newPersonLayout)
    }

}