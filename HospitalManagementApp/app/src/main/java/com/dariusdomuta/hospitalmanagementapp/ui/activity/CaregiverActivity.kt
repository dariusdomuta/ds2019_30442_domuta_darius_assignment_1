package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.show
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Caregiver
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.caregiver_activity.*
import kotlinx.android.synthetic.main.caregiver_item_layout.view.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class CaregiverActivity : BaseActivity() {

    companion object {
        val USERNAME_KEY = "username_key"

        fun newInstance(context: Context, username: String): Intent {
            val intent = Intent(context, CaregiverActivity::class.java)
            intent.putExtra(USERNAME_KEY, username)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.caregiver_activity)
        HospitalManagementApp.GRAPH.inject(this)

        setupUI()

    }

    override fun onStart() {
        super.onStart()
        findCaregiverByAccount(userStorage.getUser()?.username ?: "")
    }

    override fun onResume() {
        super.onResume()
        unlockUI()
    }


    private fun setupUI() {
        logoutButton.show()
        loggedUserName.text = userStorage.getUser()?.username

        logoutButton.setOnClickListener {
            logoutUser()
        }
    }

    private fun logoutUser() {
        userStorage.logout()
        startActivity(LoginActivity.newInstance(this))
        finish()
    }

    fun findCaregiverByAccount(username: String) {
        rx(
            hospitalManagementApi.getCaregiverByAccount(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ caregiver ->
                    hospitalManagementApi.findCaregiverPatients(caregiver?.caregiverId ?: 0)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ patients ->
                            displayCaregiver(caregiver, patients)
                        }, {
                            showToast("Couldn't load caregiver patients!")
                        })
                }, {
                    showToast("Error caregiver not found!")
                })
        )
    }

    private fun displayCaregiver(
        caregiver: Caregiver,
        patientsList: List<Patient> = arrayListOf()
    ) {
        val newPersonLayout = layoutInflater.inflate(R.layout.caregiver_item_layout, null)
        newPersonLayout.username.text = caregiver.account.username
        newPersonLayout.password.text = caregiver.account.password
        newPersonLayout.accountType.text = "CAREGIVER"
        newPersonLayout.name.text = caregiver.name
        newPersonLayout.birthDate.text = caregiver.birthDate
        newPersonLayout.address.text = caregiver.address
        newPersonLayout.gender.text = caregiver.gender.toString()
        var patientsString = ""
        patientsList.forEach {
            patientsString += it.name + ", "
        }
        if (patientsString.isEmpty()) {
            patientsString = "No patients assigned!"
        }
        newPersonLayout.patientsList.text = patientsString
        caregiversLayout.addView(newPersonLayout)
    }

}