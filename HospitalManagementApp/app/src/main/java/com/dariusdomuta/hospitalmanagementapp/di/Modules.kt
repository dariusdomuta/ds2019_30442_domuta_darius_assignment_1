package com.dariusdomuta.hospitalmanagementapp.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.dariusdomuta.hospitalmanagementapp.BuildConfig
import com.dariusdomuta.hospitalmanagementapp.business.DefaultUserStorage
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class AndroidModule(val context: Context) {

    @Provides
    fun provideContext(): Context = context.applicationContext

    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
}

@Module
class HttpModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(context: Context, userStorage: UserStorage): OkHttpClient {

        val okHttpClient = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)

        return if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClient.addInterceptor(loggingInterceptor)
            okHttpClient.build()
        } else {
            okHttpClient.build()
        }
    }

    @Provides
    @Named("apiUrl")
    fun provideApiUrl(): String {
        return BuildConfig.API_URL
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, @Named("apiUrl") apiUrl: String): Retrofit {
        val gsonConverter = GsonConverterFactory.create(GsonBuilder().create())

        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(gsonConverter)
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun provideApiErrorHandler(): ApiErrorHandler {
        return ApiErrorHandler()
    }
}

@Module
class AppModule {

    @Provides
    fun provideUserStorage(sharedPreferences: SharedPreferences): UserStorage {
        return DefaultUserStorage(sharedPreferences)
    }
}

@Module
class ApiModule {

    @Provides
    fun provideBilenniRecorderApi(retrofit: Retrofit): HospitalManagementApi {
        return retrofit.create(HospitalManagementApi::class.java)
    }
}


