package com.dariusdomuta.hospitalmanagementapp.network

import com.google.gson.annotations.SerializedName

class LoginResponse (
    @SerializedName("auth_token_is_valid")
    val authTokenIsValid: Boolean
)

class CaseCategoriesResponse(
    val data : List<CaseCategory>
)

class CaseCategory(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("created_at")
    val creationDate: String,
    @SerializedName("updated_at")
    val updateDate: String
)