package com.dariusdomuta.hospitalmanagementapp.ext

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.view.View
import android.widget.ScrollView

open class SimpleAnimatorListener : Animator.AnimatorListener {
    override fun onAnimationRepeat(animator: Animator) {
    }

    override fun onAnimationEnd(animator: Animator) {
    }

    override fun onAnimationCancel(animator: Animator) {
    }

    override fun onAnimationStart(animator: Animator) {

    }
}

fun ObjectAnimator.addStartListener(listener: () -> Unit) {
    addListener(object : SimpleAnimatorListener() {
        override fun onAnimationStart(animator: Animator) {
            super.onAnimationStart(animator)
            listener.invoke()
        }
    })
}

fun ObjectAnimator.addEndListener(listener: () -> Unit) {
    addListener(object : SimpleAnimatorListener() {
        override fun onAnimationEnd(animator: Animator) {
            super.onAnimationEnd(animator)
            listener.invoke()
        }
    })
}

fun animateFadeInFadeOut(scrollView: ScrollView, viewToAnimate: View) {
    scrollView.fullScroll(ScrollView.FOCUS_UP)
    viewToAnimate.apply {
        alpha = 0f
        visibility = View.VISIBLE

        animate()
            .alpha(1f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    viewToAnimate.animate()
                        .alpha(0f)
                        .setDuration(2000.toLong())
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                viewToAnimate.visibility = View.GONE
                            }
                        })
                }
            })
            .duration = 1000.toLong()
    }
}


