package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.*
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.activity_login.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    companion object {
        fun newInstance(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }

        private const val USER_NAME_KEY = "userName"
        private const val ACCESS_CODE_KEY = "accessCode"
    }

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    private var userName: String = ""
    private var accessCode: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        HospitalManagementApp.GRAPH.inject(this)

        setupUI()
        checkLoginSkip()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        userName = userFullName.getText().toString()
        accessCode = userAccessCode.getText().toString()

        outState.putString(USER_NAME_KEY, userName)
        outState.putString(ACCESS_CODE_KEY, accessCode)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        userName = savedInstanceState.getString(USER_NAME_KEY).toString()
        accessCode = savedInstanceState.getString(ACCESS_CODE_KEY).toString()

        userFullName.setText(userName)
        userAccessCode.setText(accessCode)
    }

    private fun setupUI() {
        logoutButton.hide()
        userAccessCode.isPasswordVisibilityToggleEnabled = true

        accessCodeEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                loginButton.performClick()
            }
            true
        }
        loginButton.setOnClickListener {
            validateAndLogin()
        }
    }

    private fun checkLoginSkip() {
        if (userStorage.isLoggedIn()) {
            startActivity(DoctorActivity.newInstance(this, userStorage.getUser()?.username ?: ""))
            finish()
        }
    }

    private fun validateAndLogin() {
        if (!userFullName.hasText()) {
            showMessage(
                mainScrollView,
                unsuccessful_submission_view,
                errorText,
                getString(R.string.error_field_mandatory)
            )
            userFullName.showError(" ")
            userFullName.requestFocus()
        } else if (!userAccessCode.hasText()) {
            userFullName.hideError()
            showMessage(
                mainScrollView,
                unsuccessful_submission_view,
                errorText,
                getString(R.string.error_field_mandatory)
            )
            userAccessCode.showError(" ")
            userAccessCode.requestFocus()
        } else {
            userName = userFullName.getText().toString()
            accessCode = userAccessCode.getText().toString()
            login(userName, accessCode)
        }
    }

    private fun login(userName: String, accessCode: String) {
        rx(
            hospitalManagementApi.login(Account(0, userName, accessCode, AccountType.CAREGIVER))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ it ->
                        userStorage.storeUser(it)
                        showToast("User ${it.username} logged in!")
                        if (it.account_type == AccountType.DOCTOR) {
                            startActivity(DoctorActivity.newInstance(this, it.username))
                        } else if (it.account_type == AccountType.CAREGIVER) {
                            startActivity(CaregiverActivity.newInstance(this, it.username))
                        } else if (it.account_type == AccountType.PATIENT) {
                            startActivity(PatientActivity.newInstance(this, it.username))
                        }

                }, {
                    showMessage(
                        mainScrollView,
                        unsuccessful_submission_view,
                        errorText,
                        apiErrorHandler.getError(this, it)
                    )
                })
        )

    }
}
