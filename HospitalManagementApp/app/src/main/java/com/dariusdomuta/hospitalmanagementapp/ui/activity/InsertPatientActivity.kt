package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.model.Gender
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.insert_patient_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class InsertPatientActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, InsertPatientActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.insert_patient_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitPatient()
        }
    }

    private fun submitPatient() {
        rx(
            hospitalManagementApi.insertPatient(getPatientData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ patients ->
                    showToast("New patient created successfully!")
                }, {
                    showToast("Error creating the new patient!")
                })
        )
    }

    private fun getPatientData(): Patient {
        return Patient(
            account = Account(
                0,
                username.getText().toString(),
                password.getText().toString(),
                AccountType.PATIENT
            ),
            name = name.getText().toString(),
            birthDate = birthDate.getText().toString(),
            gender = if (gender.getText() == "male") {
                Gender.MALE
            } else {
                Gender.FEMALE
            },
            address = address.getText().toString(),
            medicalRecord = medicalRecord.getText().toString(),
            caregiver = null,
            patientId = 0
        )
    }

}