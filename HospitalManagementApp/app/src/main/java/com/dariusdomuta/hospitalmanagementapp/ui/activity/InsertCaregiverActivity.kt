package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.model.Gender
import com.dariusdomuta.hospitalmanagementapp.model.Caregiver
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.insert_caregiver_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class InsertCaregiverActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, InsertCaregiverActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.insert_caregiver_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitCaregiver()
        }
    }

    private fun submitCaregiver() {
        rx(
            hospitalManagementApi.insertCaregiver(getCaregiverData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ caregivers ->
                    showToast("New caregiver created successfully!")
                }, {
                    showToast("Error creating the new caregiver!")
                })
        )
    }

    private fun getCaregiverData(): Caregiver {
        return Caregiver(
            account = Account(
                0,
                username.getText().toString(),
                password.getText().toString(),
                AccountType.CAREGIVER
            ),
            name = name.getText().toString(),
            birthDate = birthDate.getText().toString(),
            gender = if (gender.getText() == "male") {
                Gender.MALE
            } else {
                Gender.FEMALE
            },
            address = address.getText().toString(),
            caregiverId = 0
        )
    }
}