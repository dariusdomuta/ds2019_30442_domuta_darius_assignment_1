package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.show
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.doctor_activity.*
import javax.inject.Inject


class DoctorActivity : BaseActivity() {

    companion object {
        val USERNAME_KEY = "username_key"

        fun newInstance(context: Context, username: String): Intent {
            val intent = Intent(context, DoctorActivity::class.java)
            intent.putExtra(USERNAME_KEY, username)
            return intent
        }
    }

    @Inject
    lateinit var binelliRecorderAPI: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.doctor_activity)
        HospitalManagementApp.GRAPH.inject(this)

        setupUI()
        getAllPatients.setOnClickListener{
            startActivity(GetAllPatientsActivity.newInstance(this))
        }

        insertPatient.setOnClickListener {
            startActivity(InsertPatientActivity.newInstance(this))
        }

        getPatientById.setOnClickListener {
            startActivity(GetPatientByIdActivity.newInstance(this))
        }

        updatePatient.setOnClickListener {
            startActivity(UpdatePatientActivity.newInstance(this))
        }

        deletePatient.setOnClickListener {
            startActivity(DeletePatientActivity.newInstance(this))
        }

        getAllCaregivers.setOnClickListener {
            startActivity(GetAllCaregiversActivity.newInstance(this))
        }

        insertCaregiver.setOnClickListener {
            startActivity(InsertCaregiverActivity.newInstance(this))
        }

        getCaregiverById.setOnClickListener {
            startActivity(GetCaregiverByIdActivity.newInstance(this))
        }

        updateCaregiver.setOnClickListener {
            startActivity(UpdateCaregiverActivity.newInstance(this))
        }

        deleteCaregiver.setOnClickListener {
            startActivity(DeleteCaregiverActivity.newInstance(this))
        }

        assignCaregiverToPatient.setOnClickListener {
            startActivity(AssignCaregiverActivity.newInstance(this))
        }

        getAllMedications.setOnClickListener {
            startActivity(GetAllMedicationsActivity.newInstance(this))
        }

        getMedicationById.setOnClickListener {
            startActivity(GetMedicationByIdActivity.newInstance(this))
        }

        insertMedication.setOnClickListener {
            startActivity(InsertMedicationActivity.newInstance(this))
        }

        updateMedication.setOnClickListener {
            startActivity(UpdateMedicationActivity.newInstance(this))
        }

        deleteMedication.setOnClickListener {
            startActivity(DeleteMedicationActivity.newInstance(this))
        }
    }

    override fun onResume() {
        super.onResume()
        unlockUI()
    }


    private fun setupUI() {
        logoutButton.show()
        loggedUserName.text = userStorage.getUser()?.username

        logoutButton.setOnClickListener {
            logoutUser()
        }
    }

    private fun logoutUser() {
        userStorage.logout()
        startActivity(LoginActivity.newInstance(this))
        finish()
    }

}