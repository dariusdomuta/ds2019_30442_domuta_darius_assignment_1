package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.setText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.model.Gender
import com.dariusdomuta.hospitalmanagementapp.model.Caregiver
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.update_caregiver_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class UpdateCaregiverActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, UpdateCaregiverActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    var caregiverToUpdate: Caregiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.update_caregiver_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitCaregiver()
        }

        findCaregiver.setOnClickListener {
            findCaregiverById()
        }
    }

    fun findCaregiverById() {
        rx(
            hospitalManagementApi.findCaregiverById(caregiverId.getText()?.toInt() ?: 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ caregiver ->
                    caregiverToUpdate = caregiver
                    setCaregiverData(caregiver)
                }, {
                    showToast("Error caregiver not found!")
                })
        )
    }

    private fun submitCaregiver() {
        if (caregiverToUpdate != null) {
            rx(
                hospitalManagementApi.updateCaregiver(
                    caregiverToUpdate?.caregiverId ?: 1,
                    getCaregiverData()
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showToast("Caregiver updated successfully!")
                    }, {
                        showToast("Error updating the caregiver!")
                    })
            )
        } else {
            showToast("Please select the caregiver you want to update!")
        }
    }

    private fun setCaregiverData(caregiver: Caregiver) {
        username.setText(caregiver.account.username)
        password.setText(caregiver.account.password)
        name.setText(caregiver.name)
        birthDate.setText(caregiver.birthDate)
        gender.setText(caregiver.gender.name)
        address.setText(caregiver.address)
    }

    private fun getCaregiverData(): Caregiver {
        return Caregiver(
            account = Account(
                0,
                username.getText().toString(),
                password.getText().toString(),
                AccountType.PATIENT
            ),
            name = name.getText().toString(),
            birthDate = birthDate.getText().toString(),
            gender = if (gender.getText() == "male") {
                Gender.MALE
            } else {
                Gender.FEMALE
            },
            address = address.getText().toString(),
            caregiverId = caregiverToUpdate?.caregiverId
        )
    }

}