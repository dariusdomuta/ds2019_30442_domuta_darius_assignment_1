package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.dariusdomuta.hospitalmanagementapp.ext.animateFadeInFadeOut
import com.dariusdomuta.hospitalmanagementapp.ext.showDialogFragment
import com.dariusdomuta.hospitalmanagementapp.ui.fragment.LoadingDialogFragment
import rx.Subscription

open class BaseActivity : AppCompatActivity() {
    private val subscriptions = mutableSetOf<Subscription>()
    private val loadingDialog = LoadingDialogFragment()

    fun rx(subscription: Subscription): Subscription {
        subscriptions.add(subscription)
        return subscription
    }

    override fun onStop() {
        super.onStop()
        subscriptions.forEach { it.unsubscribe() }
    }

    fun showLoading() {
        showDialogFragment(loadingDialog)
    }

    fun dismissLoading() {
        if (loadingDialog.dialog != null) {
            loadingDialog.dismiss()
        }
    }

    fun lockUI() {
        showLoading()
        requestedOrientation =
            if (this.resources.configuration.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
    }

    fun unlockUI() {
        dismissLoading()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
    }

    fun lockScreenRotation() {
        requestedOrientation =
            if (this.resources.configuration.orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
    }

    fun showMessage(
        scrollView: ScrollView,
        viewToAnimate: View,
        textView: TextView,
        textToShow: String
    ) {
        textView.text = textToShow
        animateFadeInFadeOut(scrollView, viewToAnimate)
    }
}