package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.setText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.model.Gender
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.update_patient_activity.*
import kotlinx.android.synthetic.main.update_patient_activity.address
import kotlinx.android.synthetic.main.update_patient_activity.birthDate
import kotlinx.android.synthetic.main.update_patient_activity.gender
import kotlinx.android.synthetic.main.update_patient_activity.medicalRecord
import kotlinx.android.synthetic.main.update_patient_activity.name
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class UpdatePatientActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, UpdatePatientActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    var patientToUpdate: Patient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.update_patient_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitPatient()
        }

        findPatient.setOnClickListener {
            findPatientById()
        }
    }

    fun findPatientById() {
        rx(
            hospitalManagementApi.findPatientById(patientId.getText()?.toInt() ?: 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ patient ->
                    patientToUpdate = patient
                    setPatientData(patient)
                }, {
                    showToast("Error patient not found!")
                })
        )
    }

    private fun submitPatient() {
        if (patientToUpdate != null) {
            rx(
                hospitalManagementApi.updatePatient(
                    patientToUpdate?.patientId ?: 1,
                    getPatientData()
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showToast("Patient updated successfully!")
                    }, {
                        showToast("Error updating the patient!")
                    })
            )
        } else {
            showToast("Please select the patient you want to update!")
        }
    }

    private fun setPatientData(patient: Patient) {
        username.setText(patient.account.username)
        password.setText(patient.account.password)
        name.setText(patient.name)
        birthDate.setText(patient.birthDate)
        gender.setText(patient.gender.name)
        address.setText(patient.address)
        medicalRecord.setText(patient.medicalRecord)
    }

    private fun getPatientData(): Patient {
        return Patient(
            account = Account(
                0,
                username.getText().toString(),
                password.getText().toString(),
                AccountType.PATIENT
            ),
            name = name.getText().toString(),
            birthDate = birthDate.getText().toString(),
            gender = if (gender.getText() == "male") {
                Gender.MALE
            } else {
                Gender.FEMALE
            },
            address = address.getText().toString(),
            medicalRecord = medicalRecord.getText().toString(),
            caregiver = null,
            patientId = patientToUpdate?.patientId
        )
    }

}