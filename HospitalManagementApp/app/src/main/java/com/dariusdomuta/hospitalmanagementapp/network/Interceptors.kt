package com.dariusdomuta.hospitalmanagementapp.network

import android.content.Context
import android.content.Intent
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ui.activity.LoginActivity
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import timber.log.Timber

class AuthHeaderReplacer(val userStorage: UserStorage) : Interceptor {
    private val AUTH_HEADER = "X-Auth-Token"
    private val ACCEPT = "Accept:"
    private val APPLICATION_JSON = "application/json"

    override fun intercept(chain: Interceptor.Chain): Response {
        if (chain.request().shouldSkipAuth()) {
            return chain.proceed(chain.request())
        }
        return chain.proceed(
            chain.request().newBuilder()
                .addHeader(AUTH_HEADER, userStorage.getUserToken())
                .addHeader(ACCEPT, APPLICATION_JSON)
                .build()
        )
    }
}

class TokenExpiredInterceptor(val context: Context, val userStorage: UserStorage) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        if (arrayOf(401, 403).contains(response.code()) && !request.shouldSkipAuth()) {
            userStorage.logout()
            context.startActivity(Intent(context, LoginActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            })
            Timber.w("Session expired. Triggering logout...")
        }
        return response
    }
}

fun Request.shouldSkipAuth() = headers().names().contains("No-Auth")