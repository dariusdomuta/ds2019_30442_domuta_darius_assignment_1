package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.AccountType
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.get_patient_by_id_activity.*
import kotlinx.android.synthetic.main.person_item_layout.view.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class GetPatientByIdActivity  : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, GetPatientByIdActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.get_patient_by_id_activity)
        HospitalManagementApp.GRAPH.inject(this)

        findPatient.setOnClickListener {
            findPatientById()
        }
    }

    fun findPatientById() {
        rx(hospitalManagementApi.findPatientById(patientId.getText()?.toInt() ?: 1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ patient ->
                displayPatient(patient)
            }, {
                showToast("Error patient not found!")
            })
        )
    }

    private fun displayPatient(patient: Patient) {
        val newPersonLayout = layoutInflater.inflate(R.layout.person_item_layout, null)
        newPersonLayout.username.text = patient.account.username
        newPersonLayout.password.text = patient.account.password
        newPersonLayout.accountType.text = "PATIENT"
        newPersonLayout.name.text = patient.name
        newPersonLayout.birthDate.text = patient.birthDate
        newPersonLayout.address.text = patient.address
        newPersonLayout.gender.text = patient.gender.toString()
        newPersonLayout.medicalRecord.text = patient.medicalRecord
        newPersonLayout.caregiverName.text = patient.caregiver?.name ?: "No Caregiver Assigned"

        patientsLayout.addView(newPersonLayout)
    }
}