package com.dariusdomuta.hospitalmanagementapp.model

enum class Gender {
    MALE, FEMALE
}