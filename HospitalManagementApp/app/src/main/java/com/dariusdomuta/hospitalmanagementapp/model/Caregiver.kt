package com.dariusdomuta.hospitalmanagementapp.model

import com.google.gson.annotations.SerializedName

class Caregiver (
    @SerializedName("caregiver_id")
    val caregiverId: Int?,
    @SerializedName("name")
    val name : String,
    @SerializedName("birth_date")
    val birthDate: String,
    @SerializedName("gender")
    val gender : Gender,
    @SerializedName("address")
    val address: String,
    @SerializedName("patients")
    val patients: List<Patient>? = arrayListOf<Patient>(),
    @SerializedName("account")
    val account: Account
)