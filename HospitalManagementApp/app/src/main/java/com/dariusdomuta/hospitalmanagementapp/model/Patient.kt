package com.dariusdomuta.hospitalmanagementapp.model

import com.google.gson.annotations.SerializedName

class Patient (
    @SerializedName("patient_id")
    val patientId: Int?,
    @SerializedName("name")
    val name : String,
    @SerializedName("birth_date")
    val birthDate: String,
    @SerializedName("gender")
    val gender : Gender,
    @SerializedName("address")
    val address: String,
    @SerializedName("medical_record")
    val medicalRecord: String,
    @SerializedName("caregiver")
    val caregiver : Caregiver?,
    @SerializedName("account")
    val account: Account
)