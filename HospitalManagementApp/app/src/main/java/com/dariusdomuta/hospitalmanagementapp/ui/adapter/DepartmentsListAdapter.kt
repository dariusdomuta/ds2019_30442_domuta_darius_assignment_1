package com.dariusdomuta.hospitalmanagementapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dariusdomuta.hospitalmanagementapp.R
import kotlinx.android.synthetic.main.checked_department_item.view.*
import kotlinx.android.synthetic.main.image_layout.view.closeButton

class DepartmentsListAdapter(val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var departments: List<String> = listOf()
    var onRemoveDepartmentClick: ((String) -> Unit)? = null

    inner class DepartmentsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindActivityOnView(position: Int) {
            itemView.departmentLabel.text = departments[position]
            itemView.closeButton.setOnClickListener { onRemoveDepartmentClick?.invoke(departments[position]) }
        }
    }

    override fun getItemCount(): Int {
        return departments.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? DepartmentsViewHolder)?.bindActivityOnView(
            position
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return DepartmentsViewHolder(layoutInflater.inflate(R.layout.checked_department_item, parent, false))
    }

    fun updateDepartments(newDepartments: List<String>) {
        departments = newDepartments
        notifyDataSetChanged()
    }

    fun getCheckedDepartments(): List<String> {
        return departments
    }
}