package com.dariusdomuta.hospitalmanagementapp.model

enum class AccountType {
    DOCTOR,
    PATIENT,
    CAREGIVER,
}