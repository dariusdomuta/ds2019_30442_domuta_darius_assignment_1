package com.dariusdomuta.hospitalmanagementapp.model

import com.google.gson.annotations.SerializedName

class Doctor (
    @SerializedName("doctor_id")
    val caregiverId: Int,
    @SerializedName("name")
    val name : String,
    @SerializedName("birth_date")
    val birthDate: String,
    @SerializedName("gender")
    val gender : Gender,
    @SerializedName("address")
    val address: String,
    @SerializedName("account")
    val account: Account
)