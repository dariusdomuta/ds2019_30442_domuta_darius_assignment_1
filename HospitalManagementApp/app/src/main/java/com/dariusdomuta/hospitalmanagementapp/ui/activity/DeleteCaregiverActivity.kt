package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.delete_caregiver_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class DeleteCaregiverActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, DeleteCaregiverActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.delete_caregiver_activity)
        HospitalManagementApp.GRAPH.inject(this)

        deleteCaregiver.setOnClickListener {
            deleteCaregiver()
        }
    }

    fun deleteCaregiver() {
        rx(
            hospitalManagementApi.deleteCaregiver(caregiverId.getText()?.toInt() ?: 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showToast("Caregiver deleted successfully!")
                }, {
                    showToast("Error caregiver not found!")
                })
        )
    }
}