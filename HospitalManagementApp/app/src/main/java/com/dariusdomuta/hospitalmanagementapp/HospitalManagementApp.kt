package com.dariusdomuta.hospitalmanagementapp

import android.app.Application
import android.content.Context
import com.dariusdomuta.hospitalmanagementapp.di.AndroidModule
import com.dariusdomuta.hospitalmanagementapp.di.DIComponent
import com.dariusdomuta.hospitalmanagementapp.di.DaggerDIComponent
import timber.log.Timber
import javax.inject.Inject

class HospitalManagementApp : Application() {

    companion object {
        @JvmStatic
        lateinit var GRAPH: DIComponent

        fun createGraph(context: Context) {
            GRAPH = DaggerDIComponent.builder()
                .androidModule(AndroidModule(context))
                .build()
        }
    }

    @Inject
    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        createGraph(this)
        GRAPH.inject(this)
    }
}