package com.dariusdomuta.hospitalmanagementapp.business

import android.content.SharedPreferences
import com.dariusdomuta.hospitalmanagementapp.model.Account
import com.google.gson.Gson

interface UserStorage {

    fun isLoggedIn(): Boolean

    fun getUserToken(): String

    fun storeUserToken(token: String?)

    fun storeUser(account: Account)

    fun getUser(): Account?

    fun logout()

    fun storePrefferedDepartments(departments: List<String>)
}

class DefaultUserStorage(val sharedPreferences: SharedPreferences) : UserStorage {

    companion object {
        private val ACCESS_TOKEN_KEY = "access_token"
        private val USER_KEY = "user"
        private val DEPARTMENTS_KEY = "user_departments"
    }

    override fun isLoggedIn(): Boolean {
        return getUser() != null && !getUserToken().isBlank()
    }

    override fun getUserToken(): String {
        return sharedPreferences.getString(ACCESS_TOKEN_KEY, "") ?: ""
    }

    override fun storeUserToken(token: String?) {
        sharedPreferences.edit()
            .putString(ACCESS_TOKEN_KEY, token)
            .apply()
    }

    override fun storeUser(account: Account) {
        sharedPreferences.edit()
            .putString(USER_KEY, Gson().toJson(account))
            .apply()
    }

    override fun getUser(): Account? {
        val json = sharedPreferences.getString(USER_KEY, null)
        if (json != null) {
            return Gson().fromJson(json, Account::class.java)
        }
        return null
    }

    override fun logout() {
        sharedPreferences.edit().clear().apply()
    }

    override fun storePrefferedDepartments(departments: List<String>) {
        sharedPreferences.edit()
            .putStringSet(DEPARTMENTS_KEY, departments.toMutableSet())
            .apply()
    }
}