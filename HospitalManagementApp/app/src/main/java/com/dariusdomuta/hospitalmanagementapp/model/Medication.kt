package com.dariusdomuta.hospitalmanagementapp.model

import com.google.gson.annotations.SerializedName

class Medication (
    @SerializedName("medication_id")
    val medicationId : Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("side_effects")
    val sideEffects: String,
    @SerializedName("dosage")
    val dosage: Int
)