package com.nordlogic.binellirecorder.util

class ConstUtils {

    companion object {
        val DEPARTMENT_NAMES = listOf("Werkstatt", "Karosserie")

        val VEHICLE_ID_KEY = "vehicle_id"
        val CASE_NUMBER_KEY = "case_number"
        val CHECKED_DEPARTMENTS_KEY = "checked_departments"
        val DEPARTMENTS_KEY = "departments"
        val COMMENTS_KEY = "comments"
        val IMAGE_PATHS_KEY = "image_thumbnails"
    }

}