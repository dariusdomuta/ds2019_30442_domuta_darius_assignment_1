package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Medication
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.insert_medication_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class InsertMedicationActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, InsertMedicationActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.insert_medication_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitMedication()
        }
    }

    private fun submitMedication() {
        rx(
            hospitalManagementApi.insertMedication(getMedicationData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ medications ->
                    showToast("New medication created successfully!")
                }, {
                    showToast("Error creating the new medication!")
                })
        )
    }

    private fun getMedicationData(): Medication {
        return Medication(
            name = name.getText().toString(),
            sideEffects = sideEffects.getText().toString(),
            dosage = dosage.getText()?.toInt() ?: 3,
            medicationId = 0
        )
    }

}