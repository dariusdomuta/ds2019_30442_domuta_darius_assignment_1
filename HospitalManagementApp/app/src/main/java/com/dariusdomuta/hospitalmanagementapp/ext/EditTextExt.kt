package com.dariusdomuta.hospitalmanagementapp.ext

import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.getText(): String? {
    return editText?.text?.toString()
}

fun TextInputLayout.setText(text: String?) {
    editText?.setText(text)
}

fun EditText.textString() = text.toString()

fun TextInputLayout.hasText(): Boolean {
    return getText() != null && !getText().isNullOrBlank()
}

fun TextInputLayout.showError(errorMessage: String) {
    error = errorMessage
}

fun TextInputLayout.hideError() {
    error = null
}

