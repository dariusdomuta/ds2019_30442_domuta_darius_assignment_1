package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Caregiver
import com.dariusdomuta.hospitalmanagementapp.model.Patient
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.caregiver_item_layout.view.*
import kotlinx.android.synthetic.main.get_all_caregivers_activity.*
import kotlinx.android.synthetic.main.person_item_layout.view.accountType
import kotlinx.android.synthetic.main.person_item_layout.view.address
import kotlinx.android.synthetic.main.person_item_layout.view.birthDate
import kotlinx.android.synthetic.main.person_item_layout.view.gender
import kotlinx.android.synthetic.main.person_item_layout.view.name
import kotlinx.android.synthetic.main.person_item_layout.view.password
import kotlinx.android.synthetic.main.person_item_layout.view.username
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class GetAllCaregiversActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, GetAllCaregiversActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.get_all_caregivers_activity)
        HospitalManagementApp.GRAPH.inject(this)
    }

    override fun onStart() {
        super.onStart()

        getAllcaregivers()
    }

    fun getAllcaregivers() {
        rx(
            hospitalManagementApi.getAllCaregivers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ caregivers ->
                    caregivers.forEach { caregiver ->
                        hospitalManagementApi.findCaregiverPatients(caregiver.caregiverId ?: 0)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({patients ->
                                displaycaregiver(caregiver, patients)
                            },{
                                showToast("Couldn't load caregiver patients!")
                            })
                    }
                }, {
                    showToast("Error getting caregivers!")
                })
        )
    }

    private fun displaycaregiver(caregiver: Caregiver, patientsList: List<Patient> = arrayListOf()) {
        val newPersonLayout = layoutInflater.inflate(R.layout.caregiver_item_layout, null)
        newPersonLayout.username.text = caregiver.account.username
        newPersonLayout.password.text = caregiver.account.password
        newPersonLayout.accountType.text = "CAREGIVER"
        newPersonLayout.name.text = caregiver.name
        newPersonLayout.birthDate.text = caregiver.birthDate
        newPersonLayout.address.text = caregiver.address
        newPersonLayout.gender.text = caregiver.gender.toString()
        var patientsString = ""
        patientsList.forEach {
            patientsString += it.name + ", "
        }
        if (patientsString.isEmpty()) {
            patientsString = "No patients assigned!"
        }
        newPersonLayout.patientsList.text = patientsString
        caregiversLayout.addView(newPersonLayout)
    }
}