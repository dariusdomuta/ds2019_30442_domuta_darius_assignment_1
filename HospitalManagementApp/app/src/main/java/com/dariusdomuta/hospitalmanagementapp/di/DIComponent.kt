package com.dariusdomuta.hospitalmanagementapp.di

import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.ui.activity.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AndroidModule::class,
        HttpModule::class,
        AppModule::class,
        ApiModule::class
    )
)
interface DIComponent {
    fun inject(app: HospitalManagementApp)
    fun inject(doctorActivity: DoctorActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(getAllPatientsActivity: GetAllPatientsActivity)
    fun inject(insertPatientActivity: InsertPatientActivity)
    fun inject(getPatientByIdActivity: GetPatientByIdActivity)
    fun inject(updatePatientActivity: UpdatePatientActivity)
    fun inject(deletePatientActivity: DeletePatientActivity)
    fun inject(getAllCaregiversActivity: GetAllCaregiversActivity)
    fun inject(insertCaregiverActivity: InsertCaregiverActivity)
    fun inject(getCaregiverByIdActivity: GetCaregiverByIdActivity)
    fun inject(updateCaregiverActivity: UpdateCaregiverActivity)
    fun inject(deleteCaregiverActivity: DeleteCaregiverActivity)
    fun inject(assignCaregiverActivity: AssignCaregiverActivity)
    fun inject(caregiverActivity: CaregiverActivity)
    fun inject(patientActivity: PatientActivity)
    fun inject(getAllMedicationsActivity: GetAllMedicationsActivity)
    fun inject(getMedicationByIdActivity: GetMedicationByIdActivity)
    fun inject(insertMedicationActivity: InsertMedicationActivity)
    fun inject(updateMedicationActivity: UpdateMedicationActivity)
    fun inject(deleteMedicationActivity: DeleteMedicationActivity)
}