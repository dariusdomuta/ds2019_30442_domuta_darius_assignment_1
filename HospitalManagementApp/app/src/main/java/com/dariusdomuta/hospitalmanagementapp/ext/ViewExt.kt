package com.dariusdomuta.hospitalmanagementapp.ext

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import java.util.*

class UiExt {
    companion object {
        fun <T : View> show(vararg views: T) {
            views.filter { it.visibility != View.VISIBLE }
                    .forEach { it.visibility = View.VISIBLE }
        }

        fun <T : View> hide(vararg views: T) {
            for (view in views) {
                view.visibility = View.GONE
            }
        }


        fun <T : View> enable(vararg views: T) {
            for (view in views) {
                view.isEnabled = true
            }
        }

        fun <T : View> disable(vararg views: T) {
            for (view in views) {
                view.isEnabled = false
            }
        }
    }
}


fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun ViewGroup.getChildren(): List<View> {
    val children = ArrayList<View>()
    for (i in 0..childCount - 1) {
        children.add(getChildAt(i))
    }
    return children
}

fun ViewGroup.disable() {
    for (view: View in getChildren()) {
        if (view is ViewGroup) {
            view.disable()
        } else {
            view.isEnabled = false
        }
    }
}

fun ViewGroup.enable() {
    for (view: View in getChildren()) {
        if (view is ViewGroup) {
            view.enable()
        } else {
            view.isEnabled = true
        }
    }
}

fun EditText.addSimpleTextChangeListener(listener: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            listener(s.toString())
        }

    })
}

