package com.dariusdomuta.hospitalmanagementapp.network

import android.content.Context
import com.dariusdomuta.hospitalmanagementapp.R
import retrofit2.adapter.rxjava.HttpException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ApiError(val code: String, val message: String)

class ApiErrorHandler {

    fun getError(context: Context?, exception: Throwable): String {
        return when (exception) {
            is HttpException -> getGenericError(context)
            is SocketException, is UnknownHostException, is SocketTimeoutException -> getNetworkError(
                context
            )
            else -> getGenericError(context)
        }
    }

    private fun getGenericError(context: Context?): String {
        return context?.resources?.getString(R.string.generic_error).toString()
    }

    private fun getNetworkError(context: Context?): String {
        return context?.resources?.getString(R.string.network_error).toString()
    }

}