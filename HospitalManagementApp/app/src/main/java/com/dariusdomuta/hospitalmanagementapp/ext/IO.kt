package com.dariusdomuta.hospitalmanagementapp.ext

import android.os.Handler
import android.os.Looper

fun doOnUiThread(operation: () -> Unit) {
    Handler(Looper.getMainLooper()).post { operation.invoke() }
}

fun doSafely(operation: () -> Unit) {
    try {
        operation.invoke()
    } catch(e: Exception) {
        e.printStackTrace()
    }
}

fun doSafely(vararg operations: () -> Unit) {
    operations.forEach(::doSafely)
}

fun waitUntil(condition: () -> Boolean) {
    while (!condition.invoke()) {
        Thread.sleep(10)
    }
}

fun waitUntil(vararg conditions: () -> Boolean) {
    conditions.forEach { waitUntil { it.invoke() } }
}