package com.dariusdomuta.hospitalmanagementapp.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dariusdomuta.hospitalmanagementapp.HospitalManagementApp
import com.dariusdomuta.hospitalmanagementapp.R
import com.dariusdomuta.hospitalmanagementapp.business.UserStorage
import com.dariusdomuta.hospitalmanagementapp.ext.getText
import com.dariusdomuta.hospitalmanagementapp.ext.setText
import com.dariusdomuta.hospitalmanagementapp.ext.showToast
import com.dariusdomuta.hospitalmanagementapp.model.Medication
import com.dariusdomuta.hospitalmanagementapp.network.ApiErrorHandler
import com.dariusdomuta.hospitalmanagementapp.network.HospitalManagementApi
import kotlinx.android.synthetic.main.update_medication_activity.*
import kotlinx.android.synthetic.main.update_medication_activity.dosage
import kotlinx.android.synthetic.main.update_medication_activity.name
import kotlinx.android.synthetic.main.update_medication_activity.sideEffects
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class UpdateMedicationActivity : BaseActivity() {
    companion object {
        fun newInstance(context: Context): Intent {
            val intent = Intent(context, UpdateMedicationActivity::class.java)
            return intent
        }
    }

    @Inject
    lateinit var hospitalManagementApi: HospitalManagementApi

    @Inject
    lateinit var userStorage: UserStorage

    @Inject
    lateinit var apiErrorHandler: ApiErrorHandler

    var medicationToUpdate: Medication? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.update_medication_activity)
        HospitalManagementApp.GRAPH.inject(this)

        submitButton.setOnClickListener {
            submitMedication()
        }

        findMedication.setOnClickListener {
            findMedicationById()
        }
    }

    fun findMedicationById() {
        rx(
            hospitalManagementApi.findMedicationById(medicationId.getText()?.toInt() ?: 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ medication ->
                    medicationToUpdate = medication
                    setMedicationData(medication)
                }, {
                    showToast("Error medication not found!")
                })
        )
    }

    private fun submitMedication() {
        if (medicationToUpdate != null) {
            rx(
                hospitalManagementApi.updateMedication(
                    medicationToUpdate?.medicationId ?: 1,
                    getMedicationData()
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showToast("Medication updated successfully!")
                    }, {
                        showToast("Error updating the medication!")
                    })
            )
        } else {
            showToast("Please select the medication you want to update!")
        }
    }

    private fun setMedicationData(medication: Medication) {
        name.setText(medication.name)
        sideEffects.setText(medication.sideEffects)
        dosage.setText(medication.dosage.toString())
    }

    private fun getMedicationData(): Medication {
        return Medication(
            name = name.getText().toString(),
            sideEffects = sideEffects.getText().toString(),
            dosage = dosage.getText()?.toInt() ?: 3,
            medicationId = 0
        )
    }

}